# a script to search for different parameters.
# right now 5 parameters are included

using Distributed

if length(procs())==1
    addprocs()
end
@everywhere begin

    using Pkg
    Pkg.activate(".")

    include("src/interface-issm.jl")
    include("src/parameter_search_write_nc.jl")
end

@eval @everywhere begin
    lower=$lower
    upper=$upper
end

@everywhere begin

    const File_name = file_name
    const Upper = upper
    const Lower = lower
    const total_runs = 250

    const v_1 = 0.02*ones(total_runs) #((Upper[1].-Lower[1])*rand(MersenneTwister(),   total_runs).+Lower[1]) # i
    Random.seed!(456)
    const v_2 = exp10.(rand(Uniform(log10(Lower[2]),log10(Upper[2])),total_runs))
    Random.seed!(789)
    const v_3 = ((Upper[3].-Lower[3])*rand(MersenneTwister(),   total_runs).+Lower[3]) # k
    const v_4 = 1.5*day*ones(total_runs) #((Upper[4].-Lower[4])*rand(MersenneTwister(),   total_runs).+Lower[4]) # l
    Random.seed!(1213)
    const v_5 =  exp10.(rand(Uniform(log10(Lower[5]),log10(Upper[5])),total_runs))

end

function parameter_search_issm(pp, pg, pn, fp_save)
    wt, nr, pg_, pn_, pp_, times, a_sum_Qbs, a_sum_Qws, mean_hts, a_sum_V_source = pmap(nr->one_evaluation_issm(nr, pp, pg, pn, fp_save), 1:total_runs)
    return [wt, nr, pg_, pn_, pp_, times, a_sum_Qbs, a_sum_Qws, mean_hts, a_sum_V_source]
end
