function parameter_search_write_nc(wt, Dm,  Q_sm, smooth_mobilization, Kg, l_er, times, a_sum_Qbs, a_sum_Qws, mean_hts, a_sum_V_source,fp_save,run_name)

    fn = ("$(fp_save)$(run_name).nc")

    ### create NC ####
    ds=Dataset(fn,"c")

    ### make dims ###
    defDim(ds, "output_times",length(times))
    defDim(ds, "yearly_times",length(times)-1)
    defDim(ds, "para_dims", 1)
    defDim(ds, "spatial_dims", 1)

    ### make varaiables and write  ###
    v = defVar(ds,"wt",Float64,("spatial_dims","para_dims"))
    v[:,:] = wt
    wt=[]

    v = defVar(ds,"Dm50",Float64,("spatial_dims","para_dims"))
    v[:,:] = Dm
    Dm = []

    v = defVar(ds,"Q_sm",Float64,("spatial_dims","para_dims"))
    v[:,:] = Q_sm
    Q_sm =[]

    v = defVar(ds,"smooth_mobilization",Float64,("spatial_dims","para_dims"))
    v[:,:] = smooth_mobilization
    smooth_mobilization =[]

    v = defVar(ds,"Kg",Float64,("spatial_dims","para_dims"))
    v[:,:] = Kg
    Kg =[]

    v = defVar(ds,"l_er",Float64,("spatial_dims","para_dims"))
    v[:,:] = l_er
    l_er =[]

    v = defVar(ds,"times",Float64,("spatial_dims","output_times"))
    v[:,:] = times
    times = []

    v = defVar(ds,"a_sum_Qbs",Float64,("output_times","spatial_dims"))
    v[:,:] = a_sum_Qbs
    a_sum_Qbs =[]

    v = defVar(ds,"a_sum_Qws",Float64,("output_times","spatial_dims"))
    v[:,:] = a_sum_Qws
    a_sum_Qws =[]

    v = defVar(ds,"mean_hts",Float64,("output_times","spatial_dims"))
    v[:,:] = mean_hts
    mean_hts =[]

    v = defVar(ds,"a_sum_V_source",Float64,("output_times","spatial_dims",))
    v[:,:] = a_sum_V_source
    a_sum_V_source=[]


    close(ds)

    println("File saved @ $(fn)")

    return nothing
end
