
using Parameters, Dierckx, Interpolations, NCDatasets
using Roots
using Random
using Dates
using Statistics
using Interpolations
using Test
using Distributions
using SUGSET
using  SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, Valley, source_water_ddm, delta_h, day, year, minimize_for_DT, find_ΔT
if homedir()== "/Users/ian"
   fp_save="/Users/ian/eth/subglacial_sed/modeling/test_cases/"

elseif homedir()== "/Users/idelaney"
    fp_save="/Users/idelaney/research/erosion_transport/outputs/"
    issm_data_loc="/glaciers/"

elseif homedir()== "/home/idelaney"
    fp_save="/home/idelaney/sed_transport/issm_model_outputs/"
    issm_data_loc="/home/idelaney/data/ISSM_collapsed/"
end

file_name="updating_HO_medium_250_0.15_100_6"
issm_data = NCDatasets.Dataset("$(issm_data_loc)$(file_name).nc")

start_year = 125

#issm_time  = collect(1:1:299)#Float64.(issm_data["model_time"]*SUGSET.year) # import timestamp
issm_time = collect.(1:300)*SUGSET.year
issm_surf  = Float64.(reverse(issm_data["surface"],dims=1))  # import surface
issm_base  = Float64.(reverse(issm_data["base"]   ,dims=1))   # import base topo
issm_width = Float64.(reverse(issm_data["width"]  ,dims=1))    # import width
issm_qw    = Float64.(issm_data["SurfMelt"]) # import timestamp
relative_elev = minimum(issm_base)
issm_ub     = Float64.(reverse(issm_data["basal_velocity"],dims=1)./ SUGSET.year) #import ub
issm_length = Float64.(issm_data["flowline_dist"])# import length

###################################
### define sources and geometry ###
###################################

issm_surf_spl  = Spline2D(issm_length, issm_time, issm_surf, kx=3, ky=3) # import surface
issm_base_spl  = Spline2D(issm_length, issm_time, issm_base, kx=3, ky=3) # import base topo
issm_width_spl = Spline2D(issm_length, issm_time, issm_width,kx=3, ky=3) # import width
issm_ub_spl    = Spline2D(issm_length, issm_time, issm_ub,   kx=4, ky=3) #import ub

close(issm_data)

SUGSET.basal_velocity(s, t, pg::Valley,pp::Phys)= Dierckx.evaluate(issm_ub_spl, s, t)
SUGSET.source_till(s,t,pg::Valley,pp::Phys) = SUGSET.erosion_herman(s,t,pg,pp)
SUGSET.source_water(s,t,pg::Valley,pp::Phys) = SUGSET.source_water_ddm(s,t,pg,pp)

SUGSET.zs(s,t, pg::Valley)= max(Dierckx.evaluate(issm_surf_spl, s, t) -relative_elev, SUGSET.proglacial) # get surface from interpolation

SUGSET.zb(s,t, pg::Valley) = Dierckx.evaluate(issm_base_spl, s, t)-relative_elev# get bed from interpolations

function SUGSET.width(s,t, pg::Valley)

     if  Dierckx.evaluate(issm_width_spl,s,t) < 100
         out = 100
     else
        out = Dierckx.evaluate(issm_width_spl,s,t)
     end
    return out
end

function SUGSET.thick(s,t, pg::Valley)
    if SUGSET.zs(s,t,pg) .- SUGSET.zb(s,t,pg) < SUGSET.proglacial
        out = 0.9*SUGSET.proglacial
    else
        out = SUGSET.zs(s,t,pg) .- SUGSET.zb(s,t,pg)
    end

    return out
end


"""
gradzs(s,t,pg)
 note that this is done with Dierckx.evaluate above and below s at time t...
it seems like there is no Dierckx.derivative option. ATTENTION... Theser is some edge effects!!!
"""
function SUGSET.gradzs(s,t,pg::Valley)
    ds = (pg.sgrid[2] -pg.sgrid[1])*6.1
    value_up =   Dierckx.evaluate(issm_surf_spl, s+ds ,t)
    value_down = Dierckx.evaluate(issm_surf_spl, s-ds ,t)
    if value_up > Dierckx.evaluate(issm_surf_spl, pg.sgrid[end] ,t)
        value_up =Dierckx.evaluate(issm_surf_spl, pg.sgrid[end] ,t)
    end
    if value_down < Dierckx.evaluate(issm_surf_spl, pg.sgrid[1] ,t)
        value_down =Dierckx.evaluate(issm_surf_spl, pg.sgrid[1] ,t)
    end

    return (value_up-value_down)/(2*ds)

end

"""
gradzb(s,t,pg)
 note that this is done with Dierckx.evaluate above and below s at time t...
it seems like there is no Dierckx.derivative option.
"""
function SUGSET.gradzb(s,t,pg::Valley)
    ds = (pg.sgrid[2] -pg.sgrid[1])*6.1
    value_up =   Dierckx.evaluate(issm_base_spl, s+ds ,t)
    value_down = Dierckx.evaluate(issm_base_spl, s-ds ,t)
if value_up > Dierckx.evaluate(issm_surf_spl, pg.sgrid[end] ,t)
        value_up =Dierckx.evaluate(issm_surf_spl, pg.sgrid[end] ,t)
    end
    if value_down < Dierckx.evaluate(issm_surf_spl, pg.sgrid[1] ,t)
        value_down =Dierckx.evaluate(issm_surf_spl, pg.sgrid[1] ,t)
    end
    return  (value_up-value_down)/(2*ds)
end


pg = Valley(tspan_spin =(issm_time[start_year],issm_time[start_year+2]),
            tspan=(issm_time[start_year], issm_time[end]),
            domain = (0.0, maximum(issm_length)-850)
            )

pg = Valley(pg,
            till_growth_lim =0.25,
            till_lim =0.5,
            tout=pg.tspan[1]:0.25*day:pg.tspan[2],
            sgrid=range(0,stop=issm_length[end]-850,length=500),
            )

pp= Phys()

#################################
### implement hydrology stuff ###
#################################

# global  T_offset=issm_time[start_year-1:end].*0

# for i =1:length(T_offset)

#      println("year $(i) of $(length(T_offset))")
#      if i ==1
#          strt_val= 7.0
#      else
#          strt_val= T_offset[i-1]
#      end

#     T_offset[i] = find_zero( ΔT -> SUGSET.minimize_for_DT(ΔT, issm_qw[start_year-2+i], (issm_time[start_year-2+i], issm_time[start_year-2+i]+SUGSET.year), pg, pp,verbose=true), strt_val,  Roots.Order16())
# end

include("T_offset_$(file_name).jl")

pp = Phys(pp,
          DT= Interpolations.interpolate((issm_time[1:end],), T_offset, Interpolations.Gridded(Interpolations.Constant()))
          )

pn = Num()


function one_evaluation_issm(nr, pp, pg, pn, fp_save)
    println("Running simulation $nr of $(total_runs)")
    wt = time_ns()

    non_conservative = true

    ## redefine variables
    pp = Phys(pp,
              Dm   = v_1[nr],
              Kg   = v_2[nr],
              l_er = v_3[nr]
              )

    pg = Valley(pg,
                source_average_time = v_4[nr]
                )

    pn= Num(pn,
            smooth_mobilization=v_5[nr]
            )

    no_year = length(zeros(maximum(floor.(Ref(Int), pg.tout/year))))

    #println("\n Dm50:$(round(pp.Dm,digits=3))  K_g:$(round(pp.Kg,digits=6)) l:$(round(pp.l_er,digits=3))   Q_sm:$(round(pg.source_average_time./day,digits=3)) days  access: $(round(pn.smooth_mobilization,digits=6)) \n")


    ######################################################
    ### Find if parameter combo leads to high enough ė ###
b    ######################################################


    ė_tot = SUGSET.source_till.(pg.sgrid,Ref(pg.tout[2]),Ref(pg),Ref(pp))*SUGSET.year # yearly erosion
    pos_ind = findall(x->x > 1e-10,ė_tot)
    ė = mean(ė_tot[pos_ind])

    if ė > 0.06e-3 && ė <5.6e-3# Only realistic erosion rates... Non-alaska, non-icesheet rates from Hallet 1996
        println("Adequate Parameter Combination, Run: $(nr), Erosion rate: $(ė*1e3) mm a^-1")

        ########################
        ### spin to find ht0 ###
        ########################

        ht0_thing= zeros(length(pg.sgrid)-1)

        if non_conservative == true
            for i = 1:length(pg.sgrid)-1
                ht0_thing[i] = min(max(SUGSET.source_till(pg.sgrid[i],pg.tout[2],pg,pp)*SUGSET.year*200, 1e-3),pg.till_lim)
            end
        end

        pg =Valley(pg,
                   spin = true, # set spin == to true to call proper water function
                   ht0= ht0_thing
                   )


        ht0_spins, spin_runs = SUGSET.spinner(Valley,pg,pp,pn,0.1e-3)

        pg =Valley(pg,
                   spin = false,
                   ht0=ht0_spins,
                   tout=pg.tspan[1]:0.25*day:pg.tspan[2] # make the last year of the run
                   )

        ###########################################
        ### re-run the model with new variables ###
        ###########################################

        println("starting run_model()")
        println(now())

        sol  = run_model(pg, pp, pn)
        println("run_model() finished \n\n")

        #################
        ### post-proc ###
        #################

        hts = sol(pg.tout)
        hts = hcat(hts...)
        mean_hts_spl = Spline1D(pg.tout, Array(mean(hts, dims=1)')[:,1])

        pg =Valley(pg,
                   spin = false,
                   ht0=ht0_spins,
                   tout=pg.tspan[1]:0.25*day:pg.tspan[2] # make the last year of the run
                   )

        no_year = length(zeros(maximum(floor.(Ref(Int), pg.tout/year))))

        a_sum_V_source = zeros(no_year,1)
        a_sum_Qbs = zeros(no_year,1)
        a_sum_Qws = zeros(no_year,1)
        mean_hts  = zeros(no_year,1)

        @time _, _, sed_source, Qws, _, Qbs,_ = post_proc(sol,pg, pp, pn)

        dt= pg.tout[2]-pg.tout[1]

        for i = 1:no_year

            indies = findall(x->x > i-1 && x<i, pg.tout/year)
            a_sum_Qbs[i]= sum(Qbs[1,indies])[1].*dt
            a_sum_Qws[i]= sum(Qws[1,indies])[1].*dt
            a_sum_V_source[i] = sum(sed_source[:,indies])[1].*dt

            mean_hts[i]  = evaluate(mean_hts_spl, i*year)
        end

        ###################################
        ### will this help w/ memory??? ###
        ###################################

        sol =[]; hts =[]; sed_source=[]; Qws=[]; Qbs=[]; mean_hts_spl =[]; ht0_spins=[]; ht0_thing=[] # make big arrays go away?!


        ##################
        ### file stuff ###
        ##################

        run_name = ("$(File_name)_$(nr)_Qw_eval_ht0_0_small_thresh")

        if non_conservative == true
            run_name = ("$(File_name)_$(nr)_Qw_eval")
        end

        @time parameter_search_write_nc( wt, pp.Dm, pg.source_average_time, pn.smooth_mobilization, pp.Kg, pp.l_er, 1:no_year, a_sum_Qbs, a_sum_Qws, mean_hts, a_sum_V_source,fp_save,run_name)
        println("Last run took $( (time_ns()-wt)/1e9) \n moving on to the next")

        return (time_ns()-wt)/1e9, nr, pg, pn,  pp, 1:no_year, a_sum_Qbs, a_sum_Qws, mean_hts, a_sum_V_source
    else
        println("Low or High  Erosion Parameter Combination Run: $(nr), Erosion rate: $(ė*1e3) mm a^-1 \n Skipping")

        a_sum_Qbs = zeros(no_year); a_sum_Qws=zeros(no_year); mean_hts=zeros(no_year); a_sum_V_source = zeros(no_year)

        return nothing
    end
end
