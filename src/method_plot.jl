using PyCall
pygui(:qt5)
using PyPlot
using NCDatasets
using StatsBase
using Statistics
using HypothesisTests

const year = 86400
#################
### Load data ###
#################

if homedir()=="/home/mauro"
    fp="/home/mauro/netfs/vaw/root"*fp

elseif homedir()== "/Users/ian"
    fp_save="/Users/ian/eth/subglacial_sed/modeling/test_cases/"

elseif homedir()== "/Users/idelaney"
    fp_save="/Users/idelaney/research/erosion_transport/outputs/"
    issm_data_loc="/Users/idelaney/data/sed_modeling_data/issm_cases/"
    sugset_data_loc="/Users/idelaney/research/erosion_transport/outputs/suite_runs/"

elseif homedir()== "/home/idelaney"
    fp_save="/home/idelaney/sed_transport/issm_model_outputs/"
    issm_data_loc="/home/idelaney/data/ISSM_collapsed/"
    sugset_data_loc="/home/idelaney/data/ISSM_collapsed/"
end

#################
### ISSM data ###
#################

######################################
    fig=figure("Situation",figsize=(9,10))
    ######################################



for j = 1:2
    #j = 2

    bump_size = [0 250 ]
    deg = [ 9  6 ]


    deg = deg[j]
    bump_size=bump_size[j]

    glacier_name = ["FLAT_STEEP"
                    "BUMP_SHALLOW"
                    ]


    if deg == 9 && bump_size == 0
        glacier_name = glacier_name[1]
    elseif deg == 6 && bump_size == 250
        glacier_name = glacier_name[2]
    end


    file_name="updating_HO_medium_$(bump_size)_0.15_100_$(deg)"
    issm_data = NCDatasets.Dataset("$(issm_data_loc)$(file_name).nc")
    issm_time  = collect(1:300)
    issm_qw    = issm_data["SurfMelt"].*1.0 # import timestam
    issm_ela    = issm_data["ELA"].*1.0 # import timestamp
    issm_thick    = issm_data["thick"].*1.0 # import timestamp
    issm_ub = issm_data["basal_velocity"].*1.0 # import
    issm_width = issm_data["width"].*1.0 # import
    issm_ut = issm_data["velocity"].*1.0
    issm_surf  = reverse(issm_data["surface"],dims=1).*1.0  # import surface
    issm_base  = reverse(issm_data["base"]   ,dims=1).*1.0   # import base topo
    issm_length = issm_data["flowline_dist"].*1.0 # importlength
    issm_icevol= issm_data["IceVolume"].*1.0

    ###################
    ### SUGSET data ###
    ###################

    a_sum_Qbs = []
    a_sum_Qws = []
    a_sum_V_source = []
    avg_hts= []

    dm50=[]
    Q_sm = []
    Δσ =[]
    l_er=[]
    Kg=[]
    l_er_ncon=[]
    Kg_ncon=[]
    Δσ_ncon =[]

    files = 300

    for i = 1:files

        try
            sugset_data = NCDatasets.Dataset("$(sugset_data_loc)$(file_name)_$(i)_Qw_eval_ht0_0_small_thresh.nc")
            push!(a_sum_V_source, sugset_data["a_sum_V_source"][:,:]*1.0)
            push!(a_sum_Qbs, abs.(sugset_data["a_sum_Qbs"][:,:]*1.0))
            push!(a_sum_Qws,  abs.(sugset_data["a_sum_Qws"][:,:]*1.0))
            push!(avg_hts, sugset_data["mean_hts"][:,:]*1.0)
            push!(dm50, sugset_data["Dm50"][:,:]*1.0)
            push!(Q_sm, sugset_data["Q_sm"][:,:]*1.0)
            push!(Δσ, sugset_data["smooth_mobilization"][:,:]*1.0)
            push!(l_er,sugset_data["l_er"][:,:]*1.0)
            push!(Kg, sugset_data["Kg"][:,:]*1.0)
            close(sugset_data)

        catch

        end
        try
            sugset_data = NCDatasets.Dataset("$(sugset_data_loc)$(file_name)_$(i)_Qw_eval.nc")

            push!(l_er_ncon,sugset_data["l_er"][:,:]*1.0)
            push!(Kg_ncon, sugset_data["Kg"][:,:]*1.0)
            push!(Δσ_ncon, sugset_data["smooth_mobilization"][:,:]*1.0)
            close(sugset_data)
        catch

        end

    end
    Δσ =hcat(Δσ...)'
    l_er=hcat(l_er...)'
    Kg=hcat(Kg...)'
    Δσ_ncon =hcat(Δσ_ncon...)'
    l_er_ncon=hcat(l_er_ncon...)'
    Kg_ncon=hcat(Kg_ncon...)'

    colis_a = [0 2 4]
    colis_b = [1 3 5]

    fs =15

    #suptitle(glacier_name[1], fontsize = fs*1.2)


    ax1=plt.subplot2grid((4,3), (colis_a[j],0), rowspan=2, colspan=2)
    #plot(issm_length/1000, issm_surf[:,100]+(issm_surf[:,100]-issm_base[:,100]).*2.5,"black",linestyle="--",linewidth=1.5,label="year 1")
    fill_between(issm_length/1000, issm_base[:,end], issm_surf[:,100]+(issm_surf[:,100]-issm_base[:,100]).*5.5, color="lightskyblue",label="year 0")

    #plot(issm_length/1000, issm_surf[:,end]+(issm_surf[:,end]-issm_base[:,end]).*2.5,"black", linestyle=":", linewidth=1.5,label="year 100")
    fill_between(issm_length/1000, issm_base[:,end], issm_surf[:,end]+(issm_surf[:,end]-issm_base[:,end]).*5.5, color="navy",label="year 100")

    #    plot(issm_length/1000, issm_base[:,end],"black", linewidth=1.5,label="bed")
    fill_between(issm_length/1000, issm_base[:,end],color="wheat",label="bed")


    if j ==1
        legend(fontsize=fs*.9,loc=4)
    end
    xlim([0.1, 20])
    ylim([0, 5250])
    ylabel(L"$z$ (m)",fontsize=fs*.9)
    xlabel(L"$x$ (km)",fontsize=fs*.9)
    if j ==1
        anchored_text = annotate("a", fontsize= fs*1.5, xy=(0.025, 0.90), xycoords="axes fraction")
    else
        anchored_text = annotate("d", fontsize= fs*1.5, xy=(0.025, 0.90), xycoords="axes fraction")
    end

    anchored_text = annotate("
         $(glacier_name)
         ELAₛ: $(Int(issm_ela[1])) m
         αₓ: $(deg)°
         aₓ: $(bump_size)",
                             fontsize= fs*.9, xy=(0.2, 3700), xycoords="data")
    tick_params(axis="both" ,labelsize=fs*.8)
    #title("")

    ax2=plt.subplot2grid((4,3), (colis_a[j],2))
    semilogx(Kg,l_er,color="red", marker="*", linewidth=0)
    semilogx(Kg_ncon,l_er_ncon,color="blue", marker="*", linewidth=0)
    #fill_between(Kg,l_er)
    xlim([1e-8, 1e-4])
    xlabel(L"K$_g$ (m$^{1-l}$ a$^{l-1}$)",fontsize=fs*.9)
    ylabel(L"l",fontsize=fs*.9)
    tick_params(axis="both" ,labelsize=fs*.8)

    if j ==1
        anchored_text = annotate("b", fontsize= fs*1.5, xy=(0.025, 0.75), xycoords="axes fraction")
    else
        anchored_text = annotate("e", fontsize= fs*1.5, xy=(0.025, 0.75), xycoords="axes fraction")
    end


    ax2=plt.subplot2grid((4,3), (colis_b[j],2))
    n_bins=12

    logbins = 10 .^(range(log10(minimum(Δσ)),stop=log10(maximum(Δσ)),length=n_bins))
    n, bins, patches =hist(Δσ,bins=logbins,facecolor="red",alpha=0.75)
    logbins = 10 .^(range(log10(minimum(Δσ_ncon)),stop=log10(maximum(Δσ_ncon)),length=n_bins))
n, bins, patches =hist(Δσ_ncon,bins=logbins,facecolor="blue",alpha=0.75)
ylim([0,27])

    xlabel(L"Δσ (m^{-1})",fontsize=fs*.9)
    ylabel("Frequency",fontsize=fs*.9)
    tick_params(axis="both" ,labelsize=fs*.8)
    anchored_text = annotate("REALISTIC: $(length(Δσ_ncon))", fontsize= fs*.8, xy=(0.15, 0.85), xycoords="axes fraction",fontstyle="italic",color="blue")
anchored_text = annotate("CONSERVATIVE: $(length(Δσ))", fontsize= fs*.8, xy=(0.15, 0.7), xycoords="axes fraction",fontstyle="italic",color="red")
tight_layout()
subplots_adjust(hspace=0.2,wspace=0.2)
if j ==1
        anchored_text = annotate("c", fontsize= fs*1.5, xy=(0.025, 0.75), xycoords="axes fraction")
    else
        anchored_text = annotate("f", fontsize= fs*1.5, xy=(0.025, 0.75), xycoords="axes fraction")
    end

    tight_layout()
end
tight_layout()
savefig("/Users/idelaney/research/erosion_transport/paper/figs/glaciers.pdf")
