using PyCall
pygui(:qt5)

using PyPlot
using JLD

using Parameters
#using SUGSET
#import SUGSET: year,day
#using VAWTools
using JLD
using Dates
using Interpolations

fp = "/Users/idelaney/research/erosion_transport/outputs/"

#year = 365*86400

#include("interface-issm.jl")
@assert(file_name == "updating_HO_medium_250_0.15_100_6")

scene = ["con" "pos"]

for i = 1:length(scene)

    run_output=load("$(fp)updating_HO_medium_250_0.15_100_6_$(scene[i])_sol.jld")

    hts = run_output["sol_yearly"][:]

    hts=hcat(hts...)

    smid=zeros(length(pg.sgrid)-1)

    dx = pg.sgrid[2]-pg.sgrid[1]

    for i =length(pg.sgrid)-1:-1:1
        smid[i] = ((pg.sgrid[i+1] + pg.sgrid[i])/2) #non stagard grid
    end

    fs=13

    year_pts=[201 240 270 299]

    figure("space", figsize=(8,7))


    ax=subplot(3,1,1)
    if i ==1
        anchored_text = annotate("a", fontsize= fs*1.7, xy=(0.025, 0.75), xycoords="axes fraction")


        subplots_adjust(hspace=0.0)
        plot(pg.sgrid./1000, SUGSET.thick.(pg.sgrid,Ref(issm_time[year_pts[1]]),Ref(pg)),  linewidth=1.5,color="blue", label=L"$t_a=0$")
        plot(pg.sgrid./1000, SUGSET.thick.(pg.sgrid,Ref(issm_time[year_pts[2]]),Ref(pg)), linewidth=1.5,color="red", label=L"$t_a=30$")
        plot(pg.sgrid./1000, SUGSET.thick.(pg.sgrid,Ref(issm_time[year_pts[3]]),Ref(pg)), linewidth=1.5,color="black", label=L"$t_a=70$")
        plot(pg.sgrid./1000, SUGSET.thick.(pg.sgrid,Ref(issm_time[year_pts[4]]),Ref(pg)), linewidth=1.5,color="green", label=L"$t_a=100$")
        ylabel(L"Ice thickness ($\mathrm{m}$)",fontsize=fs)
        tick_params(axis="both",labelsize=fs*0.8)
        xlim([0, maximum(pg.sgrid./1000)])
        legend(fontsize=fs, loc=(0.1,.3))
    end

    tick_params(axis="y" ,labelsize=fs)

    ### till height
    if i ==1
        subplot(3,1,2, sharex=ax)
    else
        subplot(3,1,3, sharex=ax)
    end

    plot(smid./1000,hts[:,year_pts[1]-200].* SUGSET.width.(smid,Ref(issm_time[year_pts[1]]),Ref(pg)).*dx,  linewidth=1.5,color="blue", label=L"$t_a=0$")
    plot(smid./1000,hts[:,year_pts[2]-200].* SUGSET.width.(smid,Ref(issm_time[year_pts[2]]),Ref(pg)).*dx, linewidth=1.5,color="red", label=L"$t_a=30$")
    plot(smid./1000,hts[:,year_pts[3]-200].* SUGSET.width.(smid,Ref(issm_time[year_pts[3]]),Ref(pg)).*dx, linewidth=1.5,color="black", label=L"$t_a=70$")
    plot(smid./1000,hts[:,year_pts[4]-200].* SUGSET.width.(smid,Ref(issm_time[year_pts[4]]),Ref(pg)).*dx, linewidth=1.5,color="green", label=L"$t_a=100$")
    tick_params(axis="y" ,labelsize=fs)
    ylabel(L"$Hw$ ($\mathrm{m}^3$)",fontsize=fs)
    xlabel(L"x ($\mathrm{km}$)",fontsize=fs)
    tick_params(axis="both",labelsize=fs*0.8)
    if i ==1
        tick_params(axis="x" ,labelsize=0)
        anchored_text = annotate("b", fontsize= fs*1.7, xy=(0.025, 0.75), xycoords="axes fraction")
        anchored_text = annotate(L"CONSERVATIVE", fontsize= fs*1.4, xy=(0.1, 0.75), xycoords="axes fraction")
        setp(ax.get_xticklabels(),visible=false) # Disable x tick labels

    else
        tick_params(axis="x" ,labelsize=fs)
        anchored_text = annotate("c", fontsize= fs*1.7, xy=(0.025, 0.75), xycoords="axes fraction")
        anchored_text = annotate(L"REALISTIC", fontsize= fs*1.4, xy=(0.1, 0.75), xycoords="axes fraction")
    end

end
savefig("/Users/idelaney/research/erosion_transport/paper/figs/evolution_till_vol.pdf")
