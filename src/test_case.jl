# Bench-like synthetic run
using Dates
using Statistics
using Interpolations
using Test
using Dierckx
using Parameters
using Roots

using SUGSET
using  SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, Valley, source_water_ddm, delta_h, day, year, minimize_for_DT, find_ΔT
#########################
### read in variables ###
#########################


a= Array([ pg.tout[3299], pg.tout[10023], pg.tout[20823], pg.tout[27900], pg.tout[30000], pg.tout[40000]])

include("interface-issm.jl")

scene = ["pos" "con"]

#################
### Run Model ###
#################

const v_1 = 0.02 #Dm50
const v_4 = 1.5*day #ΔT
const total_runs = 1

if file_name == "updating_HO_medium_0_0.15_100_9"

    const v_2 = 4.1189807542929285e-7 #Kg
    const v_3 = 1.9630988385384018
    const v_5 = 0.0025416219689362875

elseif file_name == "updating_HO_medium_250_0.15_100_6"

    if scene == con
        const v_2 = 3.0636676272927426e-7
        const v_3 = 2.990312623780396
        const v_5 = 2.0367798908637257e-5
    else
        const v_2 = 4.9525058008502e-6
        const v_3 = 2.104741714166706
        const v_5 = 0.00023134951113572445
    end

end

nr=1

## redefine variables
pp = Phys(pp,
          Dm   = v_1[nr],
          Kg   = v_2[nr],
          l_er = v_3[nr]
          )

pg = Valley(pg,
            source_average_time = v_4[nr]
            )

pn= Num(pn,
        smooth_mobilization=v_5[nr]
        )
no_year = length(zeros(maximum(floor.(Ref(Int), pg.tout/year))))


println("\n Dm50:$(round(pp.Dm,digits=3))  K_g:$(round(pp.Kg,digits=6)) l:$(round(pp.l_er,digits=3))   Q_sm:$(round(pg.source_average_time./day,digits=3)) days  access: $(round(pn.smooth_mobilization,digits=6)) \n")


######################################################
### Find if parameter combo leads to high enough ė ###
######################################################


ė_tot = SUGSET.source_till.(pg.sgrid,Ref(pg.tout[2]),Ref(pg),Ref(pp))*SUGSET.year # yearly erosion
pos_ind = findall(x->x > 1e-10,ė_tot)
ė = mean(ė_tot[pos_ind])

if ė > 0.06e-3 && ė <5.6e-3# on-alaska, non-icesheet rates from Hallet 1996
    println("Adequate Parameter Combination, Run: $(nr), Erosion rate: $(ė*1e3) mm a^-1")

    ########################
    ### spin to find ht0 ###
    ########################

    ht0_thing= zeros(length(pg.sgrid)-1)
    if scene == con
        for i = 1:length(pg.sgrid)-1
            ht0_thing[i] = min(max(SUGSET.source_till(pg.sgrid[i],pg.tout[2],pg,pp)*SUGSET.year*200, 1e-3),pg.till_lim)
        end
    end

    pg =Valley(pg,
               spin = true, # set spin == to true to call proper water function
               ht0= ht0_thing
               )


    ht0_spins, spin_runs = SUGSET.spinner(Valley,pg,pp,pn,0.1e-3)



    pg =Valley(pg,
               spin = false,
               ht0=ht0_spins,
               tout=pg.tspan[1]:day:pg.tspan[2] # make the last year of the run
               )

    ###########################################
    ### re-run the model with new variables ###
    ###########################################

    println("starting run_model()")
    println(now())

    sol  = run_model(pg, pp, pn)
    println("run_model() finished \n\n")

    #################
    ### post-proc ###
    #################

    hts = sol(pg.tout)
    hts = hcat(hts...)
end

save("../issm_model_outputs/$(file_name)_$(scene)_sol.jld",
     "sol_yearly", hcat(sol(issm_time[end-100:end])...),
     "ht0", pg.ht0
     )

hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes = SUGSET.save_run(sol, pg, pp, pn, "$(file_name)",fp_save;verbose=true)
