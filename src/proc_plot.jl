##############################
### find input of sediment ###
##############################
dt = day

V_source_rate = SUGSET.extract_sed_source(sol,pg,pn,pn)

hts, dht_dts, Qws, Qws_sm, Qbs, Qbes = SUGSET.post_proc(sol,pg,pp,pn)

###################################
### plot it up -- hi resolution ###
###################################

figure("hi-resolution",figsize=(7*1.6,7))

#suptitle("Instantaneous Outputs")
ax=subplot(4,1,1)
plot(sol.t./year, V_source_rate, linewidth="2", c="black")
#axvline(x=39)
ylabel(L"$\dot{e}$ (m$^{3}$ s$^{-1}$)")
title("Instantaneous Erosion Quantity")

subplot(4,1,2, sharex=ax)
plot(pg.tout./year, -Qws[1,:], linewidth="2", c="black")
#axvline(x=39)
ylabel(L"Q$_{w}$ (m$^{3}$ s$^{-1}$)")
title("Instantaneous Water Discharge")

subplot(4,1,3, sharex=ax)
plot(pg.tout./year, -Qbs[1,:], linewidth="2", c="black")
#axvline(x=39)
ylabel(L"Q$_{s}$ (m$^{3}$ s$^{-1}$)")
title("Instantaneous Sediment Discharge")
xlabel("Date")

subplot(4,1,4, sharex=ax)
plot(pg.tout./year,Array( mean(hts, dims=1)'), linewidth="2", c="black")
#axvline(x=39)
ylabel("Till height (m)")
title("Till Height")
xlabel("Date")
tight_layout()

#######################################
### plot it up -- annual resolution ###
#######################################

Qws_spl = Spline1D(pg.tout, abs.(Qws[1,:]))
mean_hts_spl = Spline1D(pg.tout, Array(mean(hts, dims=1)')[:,1])

no_year = Int((pg.tout[end]-pg.tout[1])/year)
pts_per_year = Int(length(pg.tout)÷no_year)


a_sum_V_source = zeros(no_year,1)
a_sum_Qws = zeros(no_year,1)
a_sum_Qbs = zeros(no_year,1)
mean_hts = zeros(no_year,1)

for i=1:length(a_sum_Qbs)
    if i == 1
        t1=pg.tout[1]
    else
        t1 = pg.tout[Int((i-1)*pts_per_year)]
    end
    tend = pg.tout[Int((i)*pts_per_year)]

    _,sum_Qbs,_,a_sum_V_source[i] = SUGSET.mass_con_check(sol,pg,pp,pn,tspan=(t1,tend),dt=0.25*day)
    a_sum_Qbs[i] = abs(sum_Qbs)
    a_sum_Qws[i] = integrate(Qws_spl, t1,tend)
    mean_hts[i]  = integrate(mean_hts_spl, t1,tend)./(tend-t1)

end

figure("annual-resolution",figsize=(7*1.6,7))

#suptitle("Annual Sums")
ax=subplot(5,1,1)
plot((1:no_year).+pg.tout[1]/year, a_sum_V_source, linewidth="2", c="black")
#axvline(x=39)
ylabel(L"$\dot{e}$ (m$^{3}$ a$^{-1}$)")
title("Annual Erosion Quantity")

subplot(5,1,2, sharex=ax)
plot((1:no_year).+pg.tout[1]/year, a_sum_Qws, linewidth="2", c="black")
#axvline(x=39)
ylabel(L"Q$_{w}$ (m$^{3}$ a$^{-1}$)")
title("Annual Water Discharge")

subplot(5,1,3, sharex=ax)
plot((1:no_year).+pg.tout[1]/year, a_sum_Qbs, linewidth="2", c="black")
#axvline(x=39)
ylabel(L"Q$_{s}$ (m$^{3}$ a$^{-1}$)")
title("Annual Sediment Discharge")

subplot(5,1,4, sharex=ax)
plot((1:no_year).+pg.tout[1]/year, a_sum_Qbs./ (a_sum_V_source), linewidth="2", c="black")
axhline(y=1)
#ylim([0.0, 0.4])
ylabel(L"Q$_{s}$ ÷ $\dot{e}$ (  )")
title(L"Percent of Q$_s$ coming from that year's erosion")

subplot(5,1,5, sharex=ax)
plot((1:no_year).+pg.tout[1]/year, mean_hts, linewidth="2", c="black")
ylabel("Mean till height (m)")
xlabel("Year")
title("Annual Till Height")

tight_layout()
